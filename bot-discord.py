import discord
from discord.ext import commands
import aiohttp
import random
import sys
from dotenv import load_dotenv
import os

load_dotenv()

intents = discord.Intents.default()
intents.message_content = True
client = commands.Bot(command_prefix=os.getenv("PREFIX"), intents=intents)
client.remove_command("help")

@client.event
async def on_ready():
    await client.change_presence(status=discord.Status.online)
    print("Roleplay Bot [Discord] Bot is Ready")

@client.command()
async def help(ctx):
    embed = discord.Embed(title="HELP MENU", description="Here is my List of Commands", color=(64255))
    embed.add_field(name="MAIN", value="help - This message\nping - Checks the latency of the bot\nsay - Make me say anything\nesay - Make me say anything in an embed", inline=False)
    embed.add_field(name="UTILITIES", value="Source Code: https://gitlab.com/sonicthehedgehog/roleplaybot", inline=False)
    await ctx.send(embed=embed)

@client.command()
async def ping(ctx):
    await ctx.send("Pong")

@client.command()
async def say(ctx, *, message: commands.clean_content):
    if any(word in message for word in blockedwords):
        await ctx.send("I WILL NOT SAY ANYTHING THAT CONTAINS WORDS THAT PROMOTES OR IS RELATED TO SCAMS AND/OR ILLEGAL ACTIVITIES")
        return
    await ctx.send(f'{question}')
    await ctx.message.delete()

@say.error
async def say_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("What do you want me to say?")
    else:
        raise error

@client.command()
async def esay(ctx, *, message):
    if any(word in message for word in blockedwords):
        await ctx.send("I WILL NOT SAY ANYTHING THAT CONTAINS WORDS THAT PROMOTES OR IS RELATED TO SCAMS AND/OR ILLEGAL ACTIVITIES")
        return
    embed = discord.Embed(description=f'{question}', color=(65535))
    await ctx.send(embed=embed)
    await ctx.message.delete()

@esay.error
async def esay_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("What do you want me to say?")
    else:
        raise error

if "__main__" == __name__:
    with open("blockedwords.txt", "r") as f:
        blockedwords = f.read().splitlines()




@client.command()
async def commandnamehere(ctx, *, message):
    async with aiohttp.ClientSession() as session:
        if ctx.guild.id == YOURSERVERID:
            webhook = discord.Webhook.from_url('WEBHOOKTOKENHERE', session=session)
            await webhook.send(content=f'{message}', username="WEBHOOKNAMEHERE", avatar_url="WEBHOOKPFPURL")
            await ctx.message.delete()
        else:
            await ctx.send("This command can only be used in Sonics Bot Hub Discord Server")
            return





TOKEN = os.getenv("DISCORDBOTTOKEN")
client.run(TOKEN)
