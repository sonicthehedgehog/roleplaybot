# CUSTOM ROLEPLAY DISCORD+REVOLT BOT

This source code is for you to be able to create your own custom Roleplay discord.py bot and Revolt bot for your RP server/Channel.

DISCLAIMER: If you use the Discord version, this works better if you have one channel for RP because webhooks are involved.

## SETUP

1. Download or Fork this source code and Create your Bot on the platform of your choice.
- Discord: Create a Discord bot application at discord.com/developers and make sure Message Content intent is enabled.
- Revolt: Create a Revolt bot at https://app.revolt.chat/settings/bots
- REMEMBER: DO NOT SHARE THE TOKEN WITH ANYONE!!!!
2. Fill in everything in .env.example and rename it to .env
3. [FOR DISCORD BOT ONLY] Open bot-discord.py and at @client.command() async def commandnamehere(ctx, *, message):, here is some things to do:
- Replace commandnamehere with the command name you want to use to execute the command.
- Replace YOURSERVERID with the Server ID you want to use the bot in.
- Replace WEBHOOKTOKENHERE with your Webhook url
- Replace WEBHOOKNAMEHERE with the name of the webhook
- Replace WEBHOOKPFPURL with the url for the pfp of the webhook.
4. [FOR DISCORD BOT ONLY] For more characters, copy and paste the command where @client.command() async def commandnamehere(ctx, *, message): is and repeat step 6 for additional RP characters.
5. Install all packages and run the Bot.

to use the Revolt code, make sure your bot has the Masquerade permission as this system uses Masquerade.

## LICENSE:

We are using the MIT license so you can pretty much do whatever you want with it.